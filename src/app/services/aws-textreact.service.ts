import { Injectable } from '@angular/core';
var AWS = require('aws-sdk');
var Trestract = require('aws-sdk/clients/textract');
var Rekognition = require('aws-sdk/clients/rekognition');

@Injectable({
  providedIn: 'root',
})
export class AwsTextreactService {
  private _processedData = {};
  constructor() {
    AWS.config.update({
      region: 'us-west-1',
      credentials: {
        accessKeyId: 'AKIARO75YTFHHRG4KJHZ',
        secretAccessKey: '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',
      },
    });
    AWS.config.apiVersions = {
      textract: '2018-06-27',
      // other service API versions
    };
  }

  public setData(data) {
    this._processedData = data;
  }

  public getData() {
    return this._processedData;
  }

  public uploadDataForTextract(base64File, filename) {
    // application/pdf
    let promise = new Promise((resolve, reject) => {
      const textract = new AWS.Textract();
      let base64Data = null;
      const extension = filename.substr(filename.lastIndexOf('.') + 1);
      const fileName = `${Date.now()} - ${filename}`;
      if (extension === 'pdf') {
        let abc = base64File.replace('data:application/pdf;base64,', '');
        base64Data = Buffer.from(abc, 'base64');
      } else {
        base64Data = Buffer.from(
          base64File.replace(/^data:image\/\w+;base64,/, ''),
          'base64'
        );
      }

      const params = {
        Document: {
          /* required */
          Bytes: base64Data /* Strings will be Base-64 encoded on your behalf */,
          // S3Object: {
          //   Bucket:
          //     'textract-console-us-west-2-59976c2a-7c96-4518-ab97-ef3dd14fc76b',
          //   Name: fileName,
          // },
        },
        FeatureTypes: ['FORMS', 'TABLES'],
      };
      //  FeatureTypes: ['TABLES', 'FORMS'],
      textract.analyzeDocument(params, function (err, data) {
        if (err) {
          reject(err);
        } // an error occurred
        else {
          resolve(data);
          // console.log(data);
        } // successful response
      });
    });
    return promise;
  }

  public uploadDataForRekog(base64File, filename) {
    let promise = new Promise((resolve, reject) => {
      const rekognition = new AWS.Rekognition();
      let base64Data = null;
      const extension = filename.substr(filename.lastIndexOf('.') + 1);
      const fileName = `${Date.now()} - ${filename}`;
      if (extension === 'pdf') {
        base64Data = base64File.replace(/^data:.+;base64,/, '');
        // base64Data = Buffer.from(
        //   base64File.replace(/^data:.+;base64,/, '')
        // );
      } else {
        base64Data = Buffer.from(
          base64File.replace(/^data:image\/\w+;base64,/, ''),
          'base64'
        );
      }
      const params = {
        Image: {
          Bytes: base64Data,
        },
        MaxLabels: 100,
      };
      rekognition.detectLabels(params, function (err, data) {
        if (err) {
          reject(err);
        } // an error occurred
        else {
          resolve(data);
          // console.log(data);
        } // successful response
        // if (err) console.log(err, err.stack); // an error occurred
        // else {
        //   var table = "<table><tr><th>Low</th><th>High</th></tr>";
        //   // show each face and build out estimated age table
        //   for (var i = 0; i < data.FaceDetails.length; i++) {
        //     table += '<tr><td>' + data.FaceDetails[i].AgeRange.Low +
        //       '</td><td>' + data.FaceDetails[i].AgeRange.High + '</td></tr>';
        //   }
        //   table += "</table>";
        //   document.getElementById("opResult").innerHTML = table;
        // }
      });
    });
    return promise;
  }
}
