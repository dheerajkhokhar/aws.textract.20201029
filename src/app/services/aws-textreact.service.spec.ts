import { TestBed } from '@angular/core/testing';

import { AwsTextreactService } from './aws-textreact.service';

describe('AwsTextreactService', () => {
  let service: AwsTextreactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AwsTextreactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
