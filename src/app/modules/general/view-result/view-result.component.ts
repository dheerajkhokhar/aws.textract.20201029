import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AwsTextreactService } from 'src/app/services/aws-textreact.service';

@Component({
  selector: 'app-view-result',
  templateUrl: './view-result.component.html',
  styleUrls: ['./view-result.component.css']
})
export class ViewResultComponent implements OnInit {

  constructor(private _Activatedroute: ActivatedRoute, private _router: Router, private awsTextreactService: AwsTextreactService) { }
  public objectKeys: any = Object.keys;
  public flag = "textract";
  public rawData = null;
  public imageData = null;
  public outputHTML = null;
  public rekogLabels = [];
  public textractOutputs = { "formData": null, "tables": null, "lines": null };
  ngOnInit(): void {
    this.proceessImageData();
  }

  proceessImageData() {
    this.imageData = null;
    this.outputHTML = null;
    this.rekogLabels = [];
    this.textractOutputs = { "formData": null, "tables": null, "lines": null };
    this.rawData = this.awsTextreactService.getData();
    if (!(this.rawData && this.rawData.raw)) {
      this._router.navigate(['']);
    } else {
      if (this.rawData.type === "rekog") {
        this.imageData = this.rawData.baseFile;
        this.rekogLabels = this.rawData.raw.Labels;
        let table = '<table class="resultData"><tr><th>Object</th><th>Match %</th></tr>';
        for (let i = 0; i < this.rawData.raw.Labels.length; i++) {
          table += '<tr><td>' + this.rawData.raw.Labels[i].Name +
            '</td><td>' + Math.floor(this.rawData.raw.Labels[i].Confidence) + '</td></tr>';
        }
        table += "</table>";
        this.outputHTML = table;
      } else if (this.rawData.type === "textract") {
        this.imageData = this.rawData.baseFile;
        this.textractOutputs.formData = this.rawData.formData;
        let lines = [];
        for (let i = 0; i < this.rawData.raw.Blocks.length; i++) {
          if (this.rawData.raw.Blocks[i].BlockType === "LINE") {
            lines.push(this.rawData.raw.Blocks[i].Text);
          }
        }
        this.textractOutputs.lines = lines;
        this.textractOutputs.tables = this.rawData.tables;
        // let lines = [];
        // // show each face and build out estimated age table
        // for (let i = 0; i < this.rawData.raw.Blocks.length; i++) {
        //   if (this.rawData.raw.Blocks[i].BlockType === "LINE") {
        //     lines.push()
        //     lines += '<tr><td>' + this.rawData.raw.Blocks[i].Text + '</td></tr>';
        //   }
        // }
        // lines += '</table>';

        // let formData = '<table class="resultData"><tr><th>Key</th><th>Value</th></tr>';
        // for (let i = 0; i < Object.keys(this.rawData.formData).length; i++) {
        //   formData +=
        //     '<tr><td>' +
        //     Object.keys(this.rawData.formData)[i] +
        //     '</td><td>' +
        //     this.rawData.formData[Object.keys(this.rawData.formData)[i]] +
        //     '</td></tr>';
        // }
        // formData += '</table>';
        // this.textractOutputs.formData = formData;

        // let lines = '<table class="resultData"><tr><th>Lines</th></tr>';
        // for (let i = 0; i < this.rawData.raw.Blocks.length; i++) {
        //   if (this.rawData.raw.Blocks[i].BlockType === "LINE") {
        //     lines += '<tr><td>' + this.rawData.raw.Blocks[i].Text + '</td></tr>';
        //   }
        // }
        // lines += '</table>';
        // this.textractOutputs.lines = lines;
        
        // if(this.rawData.tables && this.rawData.tables.length > 0){
        //   this.textractOutputs.tables = this.rawData.tables;
        // } else {
        //   this.textractOutputs.tables = null;
        // }
        
      } else {
        this._router.navigate(['']);
      }
    }
  }

  ngOnDestroy() {

  }

  textractOutput($event) {
    this.proceessImageData();
  }

  rekogOutput($event) {
    this.proceessImageData();
  }

}
