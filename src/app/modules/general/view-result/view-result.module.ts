import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewResultComponent } from './view-result.component';
import { ViewResultRoutingModule } from './view-result-routing.module';
import { FormsModule } from '@angular/forms';
import { RekogModule } from 'src/app/components/rekog/rekog.module';
import { TextractModule } from 'src/app/components/textract/textract.module';
import { SafeHtmlPipeModule } from 'src/app/pipes/safeHTML/safe-html.pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ViewResultRoutingModule,
    TextractModule,
    RekogModule,
    SafeHtmlPipeModule
  ],
  exports: [
    ViewResultComponent
  ],
  declarations: [
    ViewResultComponent
  ],
  providers: [
  ],
})
export class ViewResultModule { }
