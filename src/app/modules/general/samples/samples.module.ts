import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SamplesComponent } from './samples.component';
import { SamplesRoutingModule } from './samples-routing.module';
import { FormsModule } from '@angular/forms';
import { RekogModule } from 'src/app/components/rekog/rekog.module';
import { TextractModule } from 'src/app/components/textract/textract.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SamplesRoutingModule,
    TextractModule,
    RekogModule
  ],
  exports: [
    SamplesComponent
  ],
  declarations: [
    SamplesComponent,
  ],
  providers: [
  ],
})
export class SamplesModule { }
