import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import * as textractHelper from 'aws-textract-helper';
import { AwsTextreactService } from 'src/app/services/aws-textreact.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-samples',
  templateUrl: './samples.component.html',
  styleUrls: ['./samples.component.css'],
})
export class SamplesComponent implements OnInit {
  constructor(
    private _Activatedroute: ActivatedRoute,
    private _router: Router,
    private http: HttpClient,
    private awsTextreactService: AwsTextreactService,
    private spinner: NgxSpinnerService
  ) { }
  public flag = 'textract';
  public textractSampleList = [];

  public rekogSampleList = [];

  ngOnInit(): void {
    this.textractSampleList = [
      {
        title: 'Invoices',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc1.jpg',
        imageName: 'doc1.jpg',
      },
      {
        title: 'Passport',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc2.jpg',
        imageName: 'doc2.jpg',
      },
      {
        title: 'Aadhar Card',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc3.jpg',
        imageName: 'doc3.jpg',
      },
      {
        title: 'Driving licence',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc4.jpg',
        imageName: 'doc4.jpg',
      },
    ];

    this.rekogSampleList = [
      {
        title: 'Pan Card',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc5.jpg',
        imageName: 'doc5.jpg',
      },
      {
        title: 'Receipts',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc6.jpg',
        imageName: 'doc6.jpg',
      },
      {
        title: 'Fan',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc7.jpg',
        imageName: 'doc7.jpg',
      },
      {
        title: 'Resume',
        description: 'Lorem Ipsum is simply dummy',
        uri: 'assets/images/doc8.jpg',
        imageName: 'doc8.jpg',
      },
    ];
  }

  textractOutput($event) {
    this._router.navigate(['view-result']);
  }

  rekogOutput($event) {
    this._router.navigate(['view-result']);
  }

  convertSampleImageTextract(imageName) {
    this.spinner.show();
    let imagePath = `/assets/images/${imageName}`;
    this.http.get(imagePath, { responseType: 'blob' }).subscribe((res) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        var base64data = reader.result;
        this.awsTextreactService
          .uploadDataForTextract(base64data, imageName)
          .then((result: any) => {
            const formData = textractHelper.createForm(result, {
              trimChars: [':', ' '],
            });
            const tables = textractHelper.createTables(result);
            let data = {
              raw: result,
              type: 'textract',
              baseFile: base64data,
              formData: formData,
              tables: tables,
            };
            this.awsTextreactService.setData(data);
            this.spinner.hide();
            this.textractOutput(null);
          })
          .catch((error) => {
            this.spinner.hide();
          });
      };
      reader.readAsDataURL(res);
      console.log(res);
    });
  }

  convertSampleImageRekog(imageName) {
    this.spinner.show();
    let imagePath = `/assets/images/${imageName}`;
    this.http.get(imagePath, { responseType: 'blob' }).subscribe((res) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        var base64data = reader.result;
        this.awsTextreactService
          .uploadDataForRekog(base64data, imageName)
          .then((result: any) => {
            let data = {
              raw: result,
              type: 'rekog',
              baseFile: base64data,
            };
            this.awsTextreactService.setData(data);
            this.spinner.hide();
            this.rekogOutput(null);
          })
          .catch((error) => {
            this.spinner.hide();
          });
      };
      reader.readAsDataURL(res);
      console.log(res);
    });
  }
}
