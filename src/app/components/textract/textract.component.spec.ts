import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextractComponent } from './textract.component';

describe('TextractComponent', () => {
  let component: TextractComponent;
  let fixture: ComponentFixture<TextractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextractComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
