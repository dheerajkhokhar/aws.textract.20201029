import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextractComponent } from 'src/app/components/textract/textract.component';

// import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    TextractComponent
  ],
  declarations: [
    TextractComponent,
  ],
  providers: [
  ],
})
export class TextractModule { }
