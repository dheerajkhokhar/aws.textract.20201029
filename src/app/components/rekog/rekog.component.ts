import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AwsTextreactService } from 'src/app/services/aws-textreact.service';
declare var Dropzone;
@Component({
  selector: 'app-rekog',
  templateUrl: './rekog.component.html',
  styleUrls: ['./rekog.component.css']
})
export class RekogComponent implements OnInit {

  constructor(private awsTextreactService: AwsTextreactService, private spinner: NgxSpinnerService) { }

  @ViewChild("fileInput") fileInput: ElementRef;
  @ViewChild("fileUpload") fileUpload: ElementRef;

  public base64File = null;
  public uploadFile = null;
  public fileName = null;
  public _dropzone = null;
  public progress = 0;
  private percent = 50;
  @Output() fileData = new EventEmitter();

  ngOnInit(): void {
    setTimeout(() => {
      this.enableDropzone();
    }, 100);
  }

  enableDropzone() {
    let that = this;
    let fileUpload = that.fileUpload.nativeElement;
    this._dropzone = new Dropzone(fileUpload, {
      url: "/rekog",
      acceptedFiles: ".png,.jpg,.PNG,.bmp,.BMP,.JPEG,.jpeg,.JPG,.gif,.GIF,.pdf,.PDF",
      parallelUploads: 1,
      uploadMultiple: false,
      thumbnailHeight: 120,
      thumbnailWidth: 120,
      clickable: false,
      maxFilesize: 5,
      addedfile: function (file) {
        that.setFileFromDropzone(file)
      }
    });
  }

  public setFileFromDropzone(file) {
    this.uploadFile = file;
    this.validateFileSize();
  }

  public triggerInput() {
    this.fileInput.nativeElement.click();
  }

  public readFile($event) {
    this.uploadFile = $event.target.files[0];
    this.validateFileSize();
  }

  public validateFileSize() {
    let FileSize = this.uploadFile.size / 1024 / 1024; // in MiB
    if (FileSize > 5) {
      this.uploadFile = null;
      this.percent = 0;
      this.processWithinAngularZoneForward();
    } else {
      this.percent = 100;
      this.fileName = this.uploadFile.name;
      this.processWithinAngularZoneForward();
    }
  }

  public objectDetect(): void {
    if (!this.uploadFile) {
      return
    }
    this.spinner.show();
    let file: File = this.uploadFile;
    let sFileName = file.name;
    var myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.base64File = myReader.result;
      this.awsTextreactService.uploadDataForRekog(this.base64File, sFileName).then((result: any) => {
        let data = {
          raw: result,
          type: "rekog",
          baseFile: this.base64File
        }
        this.awsTextreactService.setData(data);
        this.fileData.emit(data);
        this.spinner.hide();
      }).catch(error => {
        this.percent = 0;
        this.processWithinAngularZoneForward();
        this.spinner.hide();
      })
    };
    myReader.readAsDataURL(file);
  }



  processWithinAngularZoneForward() {
    this._increaseProgress(() => {
    });
  }

  _increaseProgress(doneCallback: () => void) {
    if (this.progress < this.percent) {
      this.progress += 2;
      window.setTimeout(() => {
        this._increaseProgress(doneCallback);
      }, 30);
    } else if (this.progress == this.percent) {
      doneCallback();
    } else {
      window.setTimeout(() => {
        this._decreaseProgress(doneCallback);
      }, 30);
    }
  }

  _decreaseProgress(doneCallback: () => void) {
    this.progress -= 2;
    if (this.progress > this.percent) {
      window.setTimeout(() => {
        this._decreaseProgress(doneCallback);
      }, 30);
    } else if (this.progress == this.percent) {
      doneCallback();
    } else {
      window.setTimeout(() => {
        this._increaseProgress(doneCallback);
      }, 30);
    }
  }

}
