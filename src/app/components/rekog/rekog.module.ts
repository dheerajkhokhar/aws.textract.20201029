import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RekogComponent } from 'src/app/components/rekog/rekog.component';
// import { TextractComponent } from 'src/app/components/textract/textract.component';

// import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    RekogComponent
  ],
  declarations: [
    RekogComponent,
  ],
  providers: [
  ],
})
export class RekogModule { }
