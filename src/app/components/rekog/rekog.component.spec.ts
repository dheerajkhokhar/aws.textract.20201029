import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RekogComponent } from './rekog.component';

describe('RekogComponent', () => {
  let component: RekogComponent;
  let fixture: ComponentFixture<RekogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RekogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RekogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
